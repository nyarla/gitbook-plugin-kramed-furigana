"use strict";

var kramed = require('kramed');
var plugin = require('./index.js');
var assert = require('assert');

try {
  assert.equal(
    kramed('{そのまま|そのまま}'),
    "<p>{そのまま|そのまま}</p>\n"
  );

  plugin.hooks.init();

  assert.equal(
    kramed('{でんでん|DenDen}'),
    "<p><ruby>でんでん<rt>DenDen</rt></ruby></p>\n"
  )
} catch (err) {
  assert.fail(err); 
}
